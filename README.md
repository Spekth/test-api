## Prueba Tecnica

Crud básico con NodeJS.

Técnologias implementadas:
* Nodejs
* Express
* MongoDB
* Mongoose

## NOTA:
Los requerimientos pedian PostgreSQL, un sistema de gestión de base de datos que no manejo. Pero que puedo aprender en cuestión de días al igual que el ORM sequelize. 


Link de la Api - Postman
* https://documenter.getpostman.com/view/12416682/VVBZQ4c2


Luego de descargar el código fuente ejecutar el siguiente comando para reconstruir los módulos de node

```
 npm install 

 ```  

 ## Run

 * npm run dev
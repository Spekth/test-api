import Departamento from '../models/departamento'

export const addDepartamento = async(req,res,next) => {
  try {
    const body = req.body;
    const departamento = await Departamento.create(body);
    res.status(200).json(departamento);
  } catch (err) {
    res.status(500).send({
      message: `An error ocurred ${err}`
   }) 
   next(err);
  }
}
export const getDepartamentos = async(req,res,next) =>{
  try {
    const departamentos = await Departamento.find({})
    res.status(200).json(departamentos);  
  } catch (err) {
    res.status(500).send({
       message: `An error ocurred ${err}`
    }) 
    next(err);
  }
}
export const getDepartamento = async(req,res,next) =>{
  try {
    let { id } = req.params; 
    const departamentoID = await Departamento.findById({id})
    .populate('empleado')
    if(!departamentoID){
      res.status(404).json({
        message:'No hay empleados'
      })
     }else{
      res.status(200).json(departamentoID); 
     }
   }catch (err) {
    res.status(500).send({
       message: `An error ocurred ${err}`
    }) 
    next(err);
  }
}
export const updateDepartamento = async(req,res,next)=>{
  try {
    const { id } = req.params;
    const update  = req.body;
    const departamento  = await Departamento.findByIdAndUpdate(id,update,{new:true});
    res.status(200).json(departamento);
  } catch (err) {
    res.status(500).send({
       message: `An error ocurred ${err}`
    }) 
    next(err);
 }
}

export const deleteDepartamento  = async(req,res,next)=>{
    try {
        const { id } = req.params;
        await Departamento.findByIdAndDelete(id);
        res.status(200).json({message:'Departament delete succesfully'});
      } catch (err) {
        res.status(500).send({
           message: `An error ocurred ${err}`
        }) 
        next(err);
    } 
}

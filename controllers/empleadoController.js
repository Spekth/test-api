import Empleado from '../models/empleado'

export const addEmpleado = async(req,res,next) => {
  try {
    const body = req.body;
    const empleado = await Empleado.create(body);
    res.status(200).json(empleado);
  } catch (err) {
    res.status(500).send({
      message: `An error ocurred ${err}`
   }) 
   next(err);
  }
}
export const getEmpleados = async(req,res,next) =>{
  try {
    const empleados = await Empleado.find({})
    .populate('codigo_departamento')
    res.status(200).json(empleados);  
  } catch (err) {
    res.status(500).send({
       message: `An error ocurred ${err}`
    }) 
    next(err);
  }
}
export const getEmpleado = async(req,res,next) =>{
  try {
    let { id } = req.params; 
    const empleadoID = await Empleado.findById({id})
    .populate('codigo_departamento')
    if(!empleadoID){
      res.status(404).json({
        message:'No hay empleados'
      })
     }else{
      res.status(200).json(empleadoID); 
     }
   }catch (err) {
    res.status(500).send({
       message: `An error ocurred ${err}`
    }) 
    next(err);
  }
}
export const updateEmpleado = async(req,res,next)=>{
  try {
    const { id } = req.params;
    const update  = req.body;
    const empleado  = await Empleado.findByIdAndUpdate(id,update,{new:true});
    res.status(200).json(empleado);
  } catch (err) {
    res.status(500).send({
       message: `An error ocurred ${err}`
    }) 
    next(err);
 }
}

export const deleteEmpleado  = async(req,res,next)=>{
    try {
        const { id } = req.params;
        // No lo elimino solo cambio su state a false por si llego a necesita el dato más adelante
        // await Empleado.findByIdAndUpdate(id,{state:false},{new:true});

        await Empleado.findByIdAndDelete(id);
        res.status(200).json({message:'Empleado delete succesfully'});
      } catch (err) {
        res.status(500).send({
           message: `An error ocurred ${err}`
        }) 
        next(err);
    } 
}

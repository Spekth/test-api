import mongoose,{Schema} from 'mongoose'

const DepartamentoSchema = new Schema({
  nombre: { type: String },
  presupuesto: { type: Number},
  state:{ type: Boolean,default:true}
},{versionKey:false})

const Departamento = mongoose.model('Departamento',DepartamentoSchema);

export default Departamento;
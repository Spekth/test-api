import mongoose,{Schema} from 'mongoose'

const EmpleadoSchema = new Schema({
  nif: { type: String },
  nombre: { type: String },
  apellido1: { type: String },
  apellido2: { type: String },
  codigo_departamento: { type: Schema.Types.ObjectId, ref:'Departamento' },
  state:{ type: Boolean,default:true}
},{versionKey:false})

const Empleado = mongoose.model('Empleado',EmpleadoSchema);

export default Empleado;
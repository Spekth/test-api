import express from 'express'
import { 
  addDepartamento,
  getDepartamentos,
  getDepartamento,
  updateDepartamento,
  deleteDepartamento } from '../controllers/departamentoController'

const router = express.Router();

router.get('/departamentos',getDepartamentos);
router.get('/departamento/:id',getDepartamento);
router.post('/departamento',addDepartamento);
router.put('/departamento/:id',updateDepartamento);
router.delete('/departamento/:id',deleteDepartamento);

export default router;
import express from 'express'
import { 
  addEmpleado,
  getEmpleados,
  getEmpleado,
  updateEmpleado,
  deleteEmpleado } from '../controllers/empleadoController'

const router = express.Router();

router.get('/empleados',getEmpleados);
router.get('/empleado/:id',getEmpleado);
router.post('/empleado',addEmpleado);
router.put('/empleado/:id',updateEmpleado);
router.delete('/empleado/:id',deleteEmpleado);

export default router;
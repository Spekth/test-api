import express from 'express'
import empleadoRouter from './empleado'
import departamentoRouter from './departamento'

const router = express.Router()

router.use(empleadoRouter)
router.use(departamentoRouter)


export default router